package br.concrete.desafio_android.request;

/**
 * Created by Fernando on 28/02/2017.
 */

public class EndPoint {

    public static final String REPOSITORIES = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%d";
    public static final String PULL_REQUESTS = "https://api.github.com/repos/%s/%s/pulls?state=all&page=%d";


}
