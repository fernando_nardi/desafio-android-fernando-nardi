package br.concrete.desafio_android.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import br.concrete.desafio_android.R;
import br.concrete.desafio_android.activity.PullRequestsActivity_;
import br.concrete.desafio_android.adapter.RepositoryAdapter;
import br.concrete.desafio_android.model.Repositories;
import br.concrete.desafio_android.model.Repository;
import br.concrete.desafio_android.request.RepositoriesRequest;

/**
 * Created by Fernando on 28/02/2017.
 */
@EFragment(R.layout.fragment_repositories)
public class RepositoriesFragment extends Fragment implements RepositoriesRequest.OnRepositories {

    @InstanceState
    public int nextPage = 1;

    @InstanceState
    public Repositories repositories;

    @ViewById(R.id.repositories)
    protected ListView listView;

    @ViewById(R.id.repositories_load)
    protected ProgressBar progressBar;

    @Bean
    protected RepositoryAdapter repositoryAdapter;

    private RepositoriesRequest repositoriesRequest;

    public static Fragment newInstance(Context context) {
        return Fragment.instantiate(context, RepositoriesFragment_.class.getName());
    }

    @AfterViews
    protected void viewsInitialized() {
        this.listView.setAdapter(this.repositoryAdapter);
        setRetainInstance(true);
        if (this.nextPage == 1) {
            this.progressBar.setVisibility(View.VISIBLE);
            this.retrieveData();
        } else {
            this.listView.setOnScrollListener(this.onScrollListener);
        }
    }

    @Override
    public void onResultRepositories(Repositories repositories) {
        this.progressBar.setVisibility(View.GONE);
        this.nextPage++;
        if (repositories != null && repositories.getRepositories().size() > 0) {
            if (this.repositories == null) {
                this.repositories = repositories;
            } else {
                this.repositories.getRepositories().addAll(repositories.getRepositories());
            }
            this.repositoryAdapter.addAll(repositories.getRepositories());
            this.repositoryAdapter.notifyDataSetChanged();
            this.listView.setOnScrollListener(this.onScrollListener);
        }
    }

    @Override
    public void onErrorResultRepositories(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        this.progressBar.setVisibility(View.GONE);
        this.listView.setOnScrollListener(this.onScrollListener);
    }

    @ItemClick(R.id.repositories)
    public void itemClickedListener(Repository repository){
        PullRequestsActivity_.intent(getActivity()).repository(repository).start();
    }

    private void retrieveData() {
        if (this.repositoriesRequest == null) {
            this.repositoriesRequest = new RepositoriesRequest(getActivity(), this);
        }
        this.repositoriesRequest.getRepositories(this.nextPage);
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if(firstVisibleItem + visibleItemCount > totalItemCount - 5){
                RepositoriesFragment.this.retrieveData();
                RepositoriesFragment.this.listView.setOnScrollListener(null);
            }
        }
    };
}
