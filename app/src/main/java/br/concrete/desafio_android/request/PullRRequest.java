package br.concrete.desafio_android.request;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;

import br.concrete.desafio_android.model.PullRequest;

/**
 * Created by Fernando on 28/02/2017.
 */

public class PullRRequest {

    private Context context;

    private Future<Response<ArrayList<PullRequest>>> responseFuture;

    private OnPullRequests onPullRequests;

    public PullRRequest(Context context, OnPullRequests onPullRequests) {
        this.context = context;
        this.onPullRequests = onPullRequests;
    }

    public void getPullRequest(String creatorUserName, String repositoryName, int page) {
        if (this.responseFuture != null) {
            this.responseFuture.cancel();
        }

        String endPoint = String.format(EndPoint.PULL_REQUESTS, creatorUserName, repositoryName, page);
        this.responseFuture = Ion.with(this.context)
                .load(endPoint)
                .as(new TypeToken<ArrayList<PullRequest>>() {})
                .withResponse()
                .setCallback(new FutureCallback<Response<ArrayList<PullRequest>>>() {

                    @Override
                    public void onCompleted(Exception e, Response<ArrayList<PullRequest>> result) {
                        if (e == null) {
                            PullRRequest.this.onPullRequests.onResultPullRequests(result.getResult());
                        } else {
                            PullRRequest.this.onPullRequests.onErrorResultPullRequests(e.getMessage());
                        }
                    }
                });
    }

    public interface OnPullRequests {
        void onResultPullRequests(ArrayList<PullRequest> pullRequests);
        void onErrorResultPullRequests(String message);
    }
}
