package br.concrete.desafio_android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fernando on 28/02/2017.
 */

public class Repository implements Parcelable {

    private int id;

    private String name;

    @SerializedName("full_name")
    private String fullName;

    private String description;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("stargazers_count")
    private int stargazersCount;

    @SerializedName("forks_count")
    private int forksCount;

    private String language;

    private Owner owner;

    protected Repository(Parcel in) {
        id = in.readInt();
        name = in.readString();
        fullName = in.readString();
        description = in.readString();
        htmlUrl = in.readString();
        stargazersCount = in.readInt();
        forksCount = in.readInt();
        language = in.readString();
        owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    public int getStargazersCount() {
        return stargazersCount;
    }

    public String getDescription() {
        return description;
    }

    public String getFullName() {
        return fullName;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getForksCount() {
        return forksCount;
    }

    public Owner getOwner() {
        return owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(fullName);
        parcel.writeString(description);
        parcel.writeString(htmlUrl);
        parcel.writeInt(stargazersCount);
        parcel.writeInt(forksCount);
        parcel.writeString(language);
        parcel.writeParcelable(owner, i);
    }
}
