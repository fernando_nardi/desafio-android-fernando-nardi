package br.concrete.desafio_android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fernando on 28/02/2017.
 */

public class Owner implements Parcelable {

    private int id;

    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    private String url;

    private String type;

    protected Owner(Parcel in) {
        id = in.readInt();
        login = in.readString();
        avatarUrl = in.readString();
        url = in.readString();
        type = in.readString();
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(login);
        parcel.writeString(avatarUrl);
        parcel.writeString(url);
        parcel.writeString(type);
    }
}
