package br.concrete.desafio_android.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.ViewById;
import java.util.ArrayList;
import br.concrete.desafio_android.R;
import br.concrete.desafio_android.adapter.PullRequestAdapter;
import br.concrete.desafio_android.model.PullRequest;
import br.concrete.desafio_android.model.Repository;
import br.concrete.desafio_android.request.PullRRequest;

/**
 * Created by Fernando on 28/02/2017.
 */

@EActivity(R.layout.activity_pull_requests)
public class PullRequestsActivity extends AppCompatActivity implements PullRRequest.OnPullRequests {

    @Extra
    protected Repository repository;

    @ViewById
    protected Toolbar toolbar;

    @ViewById(R.id.pull_requests)
    protected ListView listView;

    @InstanceState
    @NonConfigurationInstance
    public ArrayList<PullRequest> pullRequests = new ArrayList<>();

    @InstanceState
    @NonConfigurationInstance
    protected int nextPage = 1;

    @ViewById(R.id.pull_requests_load)
    protected ProgressBar progressBar;

    @Bean
    protected PullRequestAdapter pullRequestAdapter;

    private PullRRequest pullRRequest;

    @ItemClick(R.id.pull_requests)
    public void itemClickedListener(PullRequest pullRequest){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getHtmlUrl()));
        startActivity(intent);
    }

    @AfterViews
    protected void viewsInitialized() {
        this.toolbar.setTitle(this.repository.getName());
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (this.nextPage == 1) {
            this.progressBar.setVisibility(View.VISIBLE);
            this.retrieveData();
        }
        this.listView.setAdapter(this.pullRequestAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void onResultPullRequests(ArrayList<PullRequest> pullRequests) {
        this.progressBar.setVisibility(View.GONE);
        this.nextPage++;
        if (pullRequests != null && pullRequests.size() > 0) {
            this.pullRequests.addAll(pullRequests);
            this.pullRequestAdapter.notifyDataSetChanged();
            this.listView.setOnScrollListener(this.onScrollListener);
        }
    }

    @Override
    public void onErrorResultPullRequests(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        this.progressBar.setVisibility(View.GONE);
        this.listView.setOnScrollListener(this.onScrollListener);
    }

    private void retrieveData() {
        if (this.pullRRequest == null) {
            this.pullRRequest = new PullRRequest(this, this);
        }
        this.pullRRequest.getPullRequest(this.repository.getOwner().getLogin(), this.repository.getName(), this.nextPage);
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if(firstVisibleItem + visibleItemCount > totalItemCount - 5){
                PullRequestsActivity.this.listView.setOnScrollListener(null);
                PullRequestsActivity.this.retrieveData();
            }
        }
    };
}
