package br.concrete.desafio_android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;

import java.util.ArrayList;

import br.concrete.desafio_android.activity.MainActivity;
import br.concrete.desafio_android.activity.PullRequestsActivity;
import br.concrete.desafio_android.fragment.RepositoriesFragment;
import br.concrete.desafio_android.listener.OnItemClick;
import br.concrete.desafio_android.model.Repository;
import br.concrete.desafio_android.view.FooterLoadListView;
import br.concrete.desafio_android.view.FooterLoadListView_;
import br.concrete.desafio_android.view.PullRequestView;
import br.concrete.desafio_android.view.PullRequestView_;
import br.concrete.desafio_android.view.RepositoryItemView;
import br.concrete.desafio_android.view.RepositoryItemView_;

/**
 * Created by Fernando on 28/02/2017.
 */

@EBean
public class RepositoryAdapter extends BaseAdapter {

    @RootContext
    protected Context context;

    ArrayList<Repository> repositories = new ArrayList<>();

    @SystemService
    protected LayoutInflater inflater;

    @Override
    public int getCount() {
        return this.repositories.size();
    }

    @Override
    public Object getItem(int i) {
        return this.repositories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.repositories.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RepositoryItemView repositoryItemView;
        if (view == null) {
            repositoryItemView = RepositoryItemView_.build(inflater.getContext());
        } else {
            repositoryItemView = (RepositoryItemView) view;
        }

        repositoryItemView.bind(this.repositories.get(i));
        return repositoryItemView;
    }

    public void addAll(ArrayList<Repository> repositories) {
        this.repositories.addAll(repositories);
    }
}
