package br.concrete.desafio_android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Fernando on 28/02/2017.
 */

public class Repositories implements Parcelable {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("items")
    private ArrayList<Repository> repositories;

    private String message;

    private String documentation_url;

    protected Repositories(Parcel in) {
        totalCount = in.readInt();
        repositories = in.createTypedArrayList(Repository.CREATOR);
        message = in.readString();
        documentation_url = in.readString();
    }

    public static final Creator<Repositories> CREATOR = new Creator<Repositories>() {
        @Override
        public Repositories createFromParcel(Parcel in) {
            return new Repositories(in);
        }

        @Override
        public Repositories[] newArray(int size) {
            return new Repositories[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeInt(totalCount);
        parcel.writeTypedList(repositories);
        parcel.writeString(message);
        parcel.writeString(documentation_url);
    }

    public ArrayList<Repository> getRepositories() {
        return this.repositories;
    }

    public String getMessage() {
        return message;
    }
}
