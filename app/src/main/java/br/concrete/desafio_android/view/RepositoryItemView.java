package br.concrete.desafio_android.view;

import android.content.Context;
import android.net.Uri;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.concrete.desafio_android.R;
import br.concrete.desafio_android.model.Repository;

/**
 * Created by Fernando on 28/02/2017.
 */

@EViewGroup(R.layout.view_repository)
public class RepositoryItemView extends RelativeLayout {

    @ViewById(R.id.repository_name)
    protected TextView repositoryName;

    @ViewById(R.id.repository_description)
    protected TextView repositoryDescription;

    @ViewById(R.id.repository_owner_avatar)
    protected SimpleDraweeView repositoryOwnerAvatar;

    @ViewById(R.id.repository_owner_username)
    protected TextView repositoryOwnerUserName;

    @ViewById(R.id.repository_owner_first_and_last_name)
    protected TextView repositoryOwnerFirstAndLastName;

    @ViewById(R.id.repository_fork_count)
    protected TextView repositoryForkCount;

    @ViewById(R.id.repository_start_count)
    protected TextView repositoryStarCount;


    public RepositoryItemView(Context context) {
        super(context);
    }

    public void bind(Repository repository) {
        this.repositoryName.setText(repository.getName());
        this.repositoryDescription.setText(repository.getDescription());

        Uri uri = Uri.parse(repository.getOwner().getAvatarUrl());
        this.repositoryOwnerAvatar.setImageURI(uri);

        this.repositoryOwnerUserName.setText(repository.getOwner().getLogin());
        this.repositoryOwnerFirstAndLastName.setText(repository.getFullName());

        this.repositoryForkCount.setText(String.valueOf(repository.getForksCount()));
        this.repositoryStarCount.setText(String.valueOf(repository.getStargazersCount()));
    }
}
