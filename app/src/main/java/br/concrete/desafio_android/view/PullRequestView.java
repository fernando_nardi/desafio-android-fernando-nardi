package br.concrete.desafio_android.view;

import android.content.Context;
import android.net.Uri;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.concrete.desafio_android.R;
import br.concrete.desafio_android.model.PullRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Fernando on 28/02/2017.
 */

@EViewGroup(R.layout.view_pull_request)
public class PullRequestView extends RelativeLayout {

    @ViewById(R.id.pull_requests_title)
    protected TextView pullRequestTitle;

    @ViewById(R.id.pull_requests_state)
    protected TextView pullRequestState;

    @ViewById(R.id.pull_request_body)
    protected TextView pullRequestBody;

    @ViewById(R.id.pull_request_user_avatar)
    protected SimpleDraweeView pullRequestUserAvatar;

    @ViewById(R.id.pull_request_username)
    protected TextView pullRequestUserName;

    @ViewById(R.id.pull_request_user_first_and_last_name)
    protected TextView pullRequestUserFirstAndLastName;

    @ViewById(R.id.pull_request_create_at)
    protected TextView pullRequestCreatedAt;

    public PullRequestView(Context context) {
        super(context);
    }

    public void bind(PullRequest pullRequest) {
        this.pullRequestTitle.setText(pullRequest.getTitle());
        this.pullRequestState.setText(pullRequest.getState());
        this.pullRequestBody.setText(pullRequest.getBody());

        Uri uri = Uri.parse(pullRequest.getUser().getAvatarUrl());
        this.pullRequestUserAvatar.setImageURI(uri);

        this.pullRequestUserName.setText(pullRequest.getUser().getLogin());

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(pullRequest.getCreatedAt().replaceAll("Z$", "+0000"));
            this.pullRequestCreatedAt.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
