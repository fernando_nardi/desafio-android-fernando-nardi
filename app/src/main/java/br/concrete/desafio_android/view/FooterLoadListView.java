package br.concrete.desafio_android.view;

import android.content.Context;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.concrete.desafio_android.R;
import br.concrete.desafio_android.model.Repository;

/**
 * Created by Fernando on 01/03/2017.
 */

@EViewGroup(R.layout.view_footer_load)
public class FooterLoadListView extends LinearLayout {

    @ViewById(R.id.list_load_footer)
    protected ProgressBar load;

    @ViewById(R.id.list_button_try_again_footer)
    protected Button tryAgain;

    public FooterLoadListView(Context context) {
        super(context);
    }

    public void build(Repository repository) {}
}
