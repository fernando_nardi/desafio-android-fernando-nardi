package br.concrete.desafio_android.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SystemService;

import java.util.ArrayList;

import br.concrete.desafio_android.activity.PullRequestsActivity;
import br.concrete.desafio_android.model.PullRequest;
import br.concrete.desafio_android.view.PullRequestView;
import br.concrete.desafio_android.view.PullRequestView_;

/**
 * Created by Fernando on 28/02/2017.
 */

@EBean
public class PullRequestAdapter extends BaseAdapter {

    ArrayList<PullRequest> pullRequests;

    @SystemService
    protected LayoutInflater inflater;

    @AfterInject
    public void injectInFields(){
        this.pullRequests = ((PullRequestsActivity)inflater.getContext()).pullRequests;
    }

    @Override
    public int getCount() {
        return this.pullRequests.size();
    }

    @Override
    public Object getItem(int i) {
        return this.pullRequests.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.pullRequests.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        PullRequestView pullRequestView;
        if( view == null ){
            pullRequestView = PullRequestView_.build( inflater.getContext() );
        }
        else{
            pullRequestView = (PullRequestView) view;
        }

        pullRequestView.bind(this.pullRequests.get(i));
        return pullRequestView;
    }
}
