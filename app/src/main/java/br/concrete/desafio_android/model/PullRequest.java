package br.concrete.desafio_android.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Fernando on 28/02/2017.
 */

public class PullRequest implements Parcelable {

    private int id;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("closed_at")
    private String closedAt;

    private String body;

    private String title;

    @SerializedName("updated_at")
    private String updatedAt;

    private String state;

    @SerializedName("created_at")
    private String createdAt;

    private int number;

    private String url;

    private Owner user;

    protected PullRequest(Parcel in) {
        id = in.readInt();
        htmlUrl = in.readString();
        closedAt = in.readString();
        body = in.readString();
        title = in.readString();
        updatedAt = in.readString();
        state = in.readString();
        createdAt = in.readString();
        number = in.readInt();
        url = in.readString();
        user = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel in) {
            return new PullRequest(in);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(htmlUrl);
        parcel.writeString(closedAt);
        parcel.writeString(body);
        parcel.writeString(title);
        parcel.writeString(updatedAt);
        parcel.writeString(state);
        parcel.writeString(createdAt);
        parcel.writeInt(number);
        parcel.writeString(url);
        parcel.writeParcelable(user, i);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Owner getUser() {
        return user;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getState() {
        if (TextUtils.equals(this.state, "open")) {
            return "Aberto";
        } else {
            return "Fechado";
        }
    }
}
