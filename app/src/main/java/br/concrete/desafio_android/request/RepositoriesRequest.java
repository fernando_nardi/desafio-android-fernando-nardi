package br.concrete.desafio_android.request;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import br.concrete.desafio_android.model.Repositories;

/**
 * Created by Fernando on 28/02/2017.
 */

public class RepositoriesRequest {

    private Context context;

    private Future<Response<Repositories>> responseFuture;

    private OnRepositories onRepositories;

    public RepositoriesRequest(Context context, OnRepositories onRepositories) {
        this.context = context;
        this.onRepositories = onRepositories;
    }

    public void getRepositories(int page) {
        if (this.responseFuture != null) {
            this.responseFuture.cancel();
        }

        String endPoint = String.format(EndPoint.REPOSITORIES, page);
        this.responseFuture = Ion.with(this.context)
                .load(endPoint)
                .setHeader("User-Agent", "git-hub-java-pop")
                .as(new TypeToken<Repositories>() {})
                .withResponse()
                .setCallback(new FutureCallback<Response<Repositories>> () {

                    @Override
                    public void onCompleted(Exception e, Response<Repositories> result) {
                        if (e == null) {
                            if (result.getHeaders().code() == 200) {
                                RepositoriesRequest.this.onRepositories.onResultRepositories(result.getResult());
                            } else {
                                RepositoriesRequest.this.onRepositories.onErrorResultRepositories(result.getResult().getMessage());
                            }
                        } else {
                            RepositoriesRequest.this.onRepositories.onErrorResultRepositories(e.getMessage());
                        }
                    }
                });
    }

    public interface OnRepositories {
        void onResultRepositories(Repositories repositories);
        void onErrorResultRepositories(String message);
    }

}
